package messagingtool.preferences;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.swing.*;

@Getter
@Setter
@NoArgsConstructor
public class Window {

    private String name;
    private int x;
    private int y;
    private int w;
    private int h;

    public Window(String name, JFrame frame) {
        this.name = name;
        getLocationAndSizeFrom(frame);
    }

    @JsonIgnore
    public void setLocationAndSizeTo(JFrame frame) {
        frame.setSize(w, h);
        frame.setLocation(x, y);
    }

    @JsonIgnore
    public void getLocationAndSizeFrom(JFrame frame) {
        x = frame.getLocation().x;
        y = frame.getLocation().y;
        w = frame.getWidth();
        h = frame.getHeight();
    }
}
