package messagingtool.preferences;

import com.fasterxml.jackson.annotation.JsonIgnore;
import messagingtool.model.Model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Application {
    private String comment = "This file contains window and other settings used by Messaging Tool V0.1";
    private List<Window> windows = new ArrayList<>();
    private Model model;

    @JsonIgnore
    public Optional<Window> getWindow(String name) {
        return windows.stream().filter(w -> w.getName().equals(name)).findFirst();
    }

    @JsonIgnore
    public void addWindow(String name, JFrame frame) {
        windows.add(new Window(name, frame));
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
