package messagingtool.model;

import lombok.Getter;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;
import org.apache.activemq.command.ActiveMQQueue;

import javax.swing.tree.TreeNode;
import java.util.List;

@Getter
@Setter
public class Queue extends NodeElement<Queues, TreeNode> {
    private final ActiveMQQueue activeMqQueue;
    private String name;

    public Queue(Queues parent, ActiveMQQueue activeMqQueue) {
        super(parent);
        this.activeMqQueue = activeMqQueue;
        this.name = activeMqQueue.getPhysicalName();
    }

    public Queue(Queues parent, String name) {
        super(parent);
        this.activeMqQueue = null;
        this.name = name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public List<TreeNode> getChildren() {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        creator.createMenuItem("Create producer...", true, actionEvent -> menuDelegator.createActiveMQQueueProducer(getParent(), this));
        creator.createMenuItem("Create consumer...", true, actionEvent -> menuDelegator.createActiveMQQueueConsumer(getParent(), this));
        creator.createMenuItem("Refresh", true, null);
        creator.createMenuItem("Settings", true, null);
        creator.createMenuItem("Delete", true, actionEvent -> menuDelegator.deleteActiveMQQueue(getParent(),  this));
    }
}
