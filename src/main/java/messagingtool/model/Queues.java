package messagingtool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class Queues extends NodeElement<Server, Queue> {
    private ProducerConfigurations producerConfigurations = new ProducerConfigurations();
    private ConsumerConfigurations consumerConfigurations = new ConsumerConfigurations();
    @JsonIgnore
    private List<Queue> queues = new ArrayList<>();

    public Queues(Server parent) {
        super(parent);
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        creator.createMenuItem("Add", true, actionEvent -> menuDelegator.createActiveMQQueue(this));
        creator.createMenuItem("Empty all", true, null);
        creator.createMenuItem("Refresh", true, null);
    }

    @Override
    public String getDisplayName() {
        return "Queues";
    }

    @Override
    public List<Queue> getChildren() {
        return queues;
    }

    @Override
    public boolean isLeaf() {
        return queues.isEmpty();
    }

    public void prepareAfterDeserialization(Server server) {
        setParent(server);
    }
}
