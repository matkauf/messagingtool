package messagingtool.model;

import lombok.Getter;

public enum ServerType {

    ACTIVEMQ("ActiveMQ"), WEBSPHEREMQ("Websphere MQ"), KAFKA("Apache Kafka");

    @Getter
    private final String displayName;

    ServerType(String displayName) {
        this.displayName = displayName;
    }
}
