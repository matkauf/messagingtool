package messagingtool.model;

import javax.swing.*;

public class AbstractConfiguration {
    private int x;
    private int y;
    private int w;
    private int h;

    public void updateLocationAndSizeFrom(JFrame frame) {
        x = frame.getX();
        y = frame.getY();
        w = frame.getWidth();
        h = frame.getHeight();
    }

    public void setLocationAndSizeTo(JFrame frame) {
        frame.setLocation(x, y);
        if (w > 0 && h > 0) {
            frame.setSize(w, h);
        }
    }

}
