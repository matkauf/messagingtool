package messagingtool.model;

import lombok.Getter;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;
import org.apache.activemq.command.ActiveMQTopic;

import javax.swing.tree.TreeNode;
import java.util.List;

@Getter
@Setter
public class Topic extends NodeElement<Topics, TreeNode> {
    private final ActiveMQTopic activeMQTopic;
    private String name;

    public Topic(Topics parent, ActiveMQTopic activeMQTopic) {
        super(parent);
        this.activeMQTopic = activeMQTopic;
        this.name = activeMQTopic.getPhysicalName();
    }

    public Topic(Topics parent, String name) {
        super(parent);
        this.activeMQTopic = null;
        this.name = name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public List<TreeNode> getChildren() {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        creator.createMenuItem("Create producer...", true, actionEvent -> menuDelegator.createActiveMQTopicProducer(getParent(), this));
        creator.createMenuItem("Create consumer...", true, actionEvent -> menuDelegator.createActiveMQTopicConsumer(getParent(), this));
        creator.createMenuItem("Refresh", true, null);
        creator.createMenuItem("Settings", true, null);
        creator.createMenuItem("Delete", true, actionEvent -> menuDelegator.deleteActiveMQTopic(getParent(),  this));
    }
}
