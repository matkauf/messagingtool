package messagingtool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class ProducerConfigurations {
    private List<ProducerConfiguration> configurations = new ArrayList<>();

    @JsonIgnore
    public Optional<ProducerConfiguration> getByDestination(String name) {
        return configurations.stream().filter(c -> name.equals(c.getName())).findFirst();
    }

    @JsonIgnore
    public String getMessageTextByDestination(String name) {
        return getByDestination(name).map(ProducerConfiguration::getMessageText).orElse("");
    }

    @JsonIgnore
    public ProducerConfiguration createOrGetByDestination(String name) {
        return getByDestination(name).orElseGet(() -> {
            ProducerConfiguration config = new ProducerConfiguration();
            config.setName(name);
            configurations.add(config);
            return config;
        });
    }
}
