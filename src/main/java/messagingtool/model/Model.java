package messagingtool.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Model extends NodeElement<TreeNode, Server> {
    private List<Server> servers = new ArrayList<>();

    @Override
    public String getDisplayName() {
        return "Brokers";
    }

    @Override
    public List<Server> getChildren() {
        return servers;
    }

    @Override
    public boolean isLeaf() {
        return servers.size() == 0;
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        creator.createMenuItem("Add ActiveMQ broker...", true, actionEvent -> menuDelegator.createActiveMQBroker());
        creator.createMenuItem("Add WebsphereMQ connection manager...", true, null);
        creator.createMenuItem("Refresh", true, null);
    }

    public Model prepareAfterDeserialization() {
        setParent(null);
        servers.forEach(s -> s.prepareAfterDeserialization(this));
        return this;
    }
}
