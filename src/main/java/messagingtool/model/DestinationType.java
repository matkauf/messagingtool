package messagingtool.model;

import lombok.Getter;

@Getter
public enum DestinationType {

    Queue("Queue", "queue", "QUEUE"),
    Topic("Topic", "topic", "TOPIC");


    private final String uppercaseDescription;
    private final String lowercaseDescription;
    private final String serializationDescription;

    DestinationType(String uppercaseDescription, String lowercaseDescription, String serializationDescription) {
        this.uppercaseDescription = uppercaseDescription;
        this.lowercaseDescription = lowercaseDescription;
        this.serializationDescription = serializationDescription;
    }

    public boolean isQueue() {
        return this == Queue;
    }

}
