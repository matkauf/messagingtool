package messagingtool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class Topics extends NodeElement<Server, Topic> {
    private ProducerConfigurations producerConfigurations = new ProducerConfigurations();
    private ConsumerConfigurations consumerConfigurations = new ConsumerConfigurations();
    @JsonIgnore
    private List<Topic> topics = new ArrayList<>();

    public Topics(Server parent) {
        super(parent);
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        creator.createMenuItem("Add", true, actionEvent -> menuDelegator.createActiveMQTopic(this));
        creator.createMenuItem("Refresh", true, null);
    }

    @Override
    public String getDisplayName() {
        return "Topics";
    }

    @Override
    public List<Topic> getChildren() {
        return topics;
    }

    @Override
    public boolean isLeaf() {
        return topics.isEmpty();
    }

    public void prepareAfterDeserialization(Server server) {
        setParent(server);
    }
}
