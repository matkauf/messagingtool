package messagingtool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class ConsumerConfigurations {
    private List<ConsumerConfiguration> configurations = new ArrayList<>();

    @JsonIgnore
    public Optional<ConsumerConfiguration> getByDestination(String name) {
        return configurations.stream().filter(c -> name.equals(c.getName())).findFirst();
    }


    @JsonIgnore
    public ConsumerConfiguration createOrGetByDestination(String name) {
        return getByDestination(name).orElseGet(() -> {
            ConsumerConfiguration config = new ConsumerConfiguration();
            config.setName(name);
            configurations.add(config);
            return config;
        });
    }
}
