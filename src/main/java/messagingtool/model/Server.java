package messagingtool.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;
import messagingtool.messaging.activemq.ActiveMqEmbeddedServer;

import javax.swing.tree.TreeNode;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class Server extends NodeElement<Model, TreeNode> {
    private String name;
    private Queues queues;
    private Topics topics;
    private ServerType serverType;

    private String host = "";
    private int port;
    private String defaultUsername;
    private String defaultPassword;

    @JsonIgnore
    private ActiveMqEmbeddedServer activeMqEmbeddedServer;
    @JsonIgnore
    private boolean connected = false;


    public Server(Model parent, ServerType serverType) {
        super(parent);
        this.serverType = serverType;
        queues = new Queues(this);
        topics = new Topics(this);
    }

    @Override
    public String getDisplayName() {
        return serverType.getDisplayName() + " broker "
                + ((name == null || name.isBlank()) ? host + ":" + port : name)
                + (!isEmbedded() ? "" : isConnected() ? " [EMBEDDED] [RUNNING...]" : " [EMBEDDED] [STOPPED]");
    }

    @Override
    public List<TreeNode> getChildren() {
        return Arrays.asList(queues, topics);
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @JsonIgnore
    public boolean isEmbedded() {
        return host.trim().toLowerCase().equals("localhost");
    }

    @JsonIgnore
    public boolean isConnected() {
        return activeMqEmbeddedServer != null || connected;
    }

    @JsonIgnore
    public String getHostAndPort() {
        return host + ":" + port;
    }

    @Override
    public void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator) {
        if (isConnected()) {
            creator.createMenuItem(isEmbedded() ? "Disconnect & Stop" : "Disconnect", true, actionEvent -> menuDelegator.closeActiveMQBroker(this));
        } else {
            creator.createMenuItem(isEmbedded() ? "Start & Connect" : "Connect", true, actionEvent -> menuDelegator.openActiveMQBroker(this));
        }
        creator.createMenuItem("Refresh", isConnected(), actionEvent -> menuDelegator.refreshActiveMQQueuesAndTopics(this));
        creator.createMenuItem("Settings", !isConnected(), actionEvent -> menuDelegator.updateActiveMQBroker(this));
        creator.createMenuItem("Remove", !isConnected(), null);
    }

    public void prepareAfterDeserialization(Model parent) {
        setParent(parent);
        queues.prepareAfterDeserialization(this);
        topics.prepareAfterDeserialization(this);
    }
}
