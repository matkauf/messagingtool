package messagingtool.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class ConsumerConfiguration extends AbstractConfiguration {
    private String name;
    private String displayType = "";
    private boolean listening = false;
}
