package messagingtool.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.gui.component.NodeElement;
import messagingtool.gui.component.PopupMenuCreator;

import javax.swing.tree.TreeNode;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor // used only for deserialization
public class ProducerConfiguration extends AbstractConfiguration {
    private String name;
    private String messageText;
}
