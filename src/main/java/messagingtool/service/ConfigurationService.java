package messagingtool.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import messagingtool.preferences.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConfigurationService {

    @Autowired
    private final ObjectMapper objectMapper;

    @Getter
    private Application application;

    public void load() {
        Path path = getConfigurationFileLocation();
        try {
            String s = Files.readString(path);
            application = objectMapper.readValue(s, Application.class);
            log.info("Configuration read from {}", path);
        } catch (Exception e) {
            log.warn("Could not read configuration from path " + path + " due to " + e.toString());
            log.info("Will use new default configuration");
            application = new Application();
        }
    }

    public void save() {
        Path path = getConfigurationFileLocation();
        try {
            String s = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(application);
            Files.writeString(path, s);
            log.info("Configuration saved to {}", path);
        } catch (Exception e) {
            log.error("Could not save configuration to " + path + " due to: ", e);
        }
    }


    private Path getConfigurationFileLocation() {
        return Paths.get(System.getProperty("user.home")
                + System.getProperty("file.separator")
                + ".messagingtool.conf");
    }
}



