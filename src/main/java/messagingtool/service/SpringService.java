package messagingtool.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.component.WindowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Initialized by Spring to get access to all the other spring based services. May be obsolete later.
 */
@Service
@Slf4j
@RequiredArgsConstructor
@Getter
public class SpringService {
    @Autowired
    private final ConfigurationService configurationService;

    @Autowired
    private final WindowService windowService;

    @Autowired
    private final PrettyPrinter prettyPrinter;

}
