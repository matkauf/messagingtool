package messagingtool.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Service;

import java.io.StringWriter;

@Service
@AllArgsConstructor
public class PrettyPrinter {
    private static final int INDENT = 4;
    private static final boolean IGNORE_DECLARATION = false;
    private final ObjectMapper mapper;


    public String getXmlPrettyPrinted(String xml) {
        try {

            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setIndentSize(INDENT);
            format.setSuppressDeclaration(IGNORE_DECLARATION);
            format.setEncoding("UTF-8");
            format.setNewLineAfterDeclaration(false);

            org.dom4j.Document document = DocumentHelper.parseText(xml);
            StringWriter sw = new StringWriter();
            XMLWriter writer = new XMLWriter(sw, format);
            writer.write(document);
            return sw.toString();

        } catch (Exception e) {
            return xml;
        }
    }

    public String getJsonPrettyPrinted(String json) {
        try {
            JsonNode jsonNode = mapper.readValue(json, JsonNode.class);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        } catch (Exception e) {
            return json;
        }
    }
}
