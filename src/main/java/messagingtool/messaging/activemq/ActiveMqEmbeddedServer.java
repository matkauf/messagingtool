package messagingtool.messaging.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.model.Server;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;

import java.io.File;
import java.net.InetAddress;
import java.net.URI;

@Slf4j
public class ActiveMqEmbeddedServer {

    private BrokerService broker;

    public void start(Server server) throws Exception {
        broker = new BrokerService();
        broker.setAdvisorySupport(true);
        String hostname = InetAddress.getLocalHost().getHostName();
        String canonicalHostname = InetAddress.getLocalHost().getCanonicalHostName();
        addConnector("localhost", server.getPort());
        addConnector(hostname, server.getPort());
        if (!hostname.equals(canonicalHostname)) {
            addConnector(canonicalHostname, server.getPort());
        }
        String dir = "localhost_" + server.getPort();
        broker.getPersistenceAdapter().setDirectory(new File("activemq-data/" + dir + "/persistence_adapter"));
        broker.setBrokerName(dir);
        broker.start();
    }

    private void addConnector(String hostname, int port) throws Exception {
        TransportConnector connector = new TransportConnector();
        String uri = "tcp://" + hostname + ":" + port;
        log.info("Connector added: {}", uri);
        connector.setUri(new URI(uri));
        broker.addConnector(connector);
    }

    public boolean isRunning() {
        return !broker.isStopped() && !broker.isStopping();
    }

    public void shutdown() throws Exception {
        broker.stop();
    }

}
