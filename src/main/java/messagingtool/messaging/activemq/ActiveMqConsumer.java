package messagingtool.messaging.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.model.DestinationType;
import messagingtool.model.Server;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public class ActiveMqConsumer implements ExceptionListener, MessageListener {

    private final Server server;
    private final String destinationName;
    private final DestinationType destinationType;
    private final LinkedBlockingQueue<Message> messages;
    private ActiveMQConnection connection;
    private Session session;
    private MessageConsumer consumer;

    public ActiveMqConsumer(Server server, String destinationName, DestinationType destinationType, LinkedBlockingQueue<Message> messages) {
        this.server = server;
        this.destinationName = destinationName;
        this.destinationType = destinationType;
        this.messages = messages;
    }

    public void prepare() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + server.getHost() + ":" + server.getPort());
            connectionFactory.setUserName(server.getDefaultUsername());
            connectionFactory.setPassword(server.getDefaultPassword());
            connection = (ActiveMQConnection) connectionFactory.createConnection();
            connection.setExceptionListener(this);

            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = destinationType.isQueue() ? session.createQueue(destinationName) : session.createTopic(destinationName);
            consumer = session.createConsumer(destination);
            consumer.setMessageListener(this);
        } catch (JMSException e) {
            onException(e);
        }
    }

    public void start() {
        try {
            connection.start();
        } catch (JMSException e) {
            log.error("Caught: ", e);
            onException(e);
        }
    }

    public void stop() {
        try {
            connection.stop();
        } catch (JMSException e) {
            log.error("Caught: ", e);
            onException(e);
        }
    }

    public void shutdown() {
        try {
            consumer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            onException(e);
        }
    }

    @Override
    public void onMessage(Message message) {
        messages.offer(message);
    }

    @Override
    public synchronized void onException(JMSException ex) {
        log.error("Caught: ", ex);
        log.error("JMS Exception occured.  Shutting down client.");
    }
}
