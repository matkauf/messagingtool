package messagingtool.messaging.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.model.DestinationType;
import messagingtool.model.Server;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;

import javax.jms.*;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class ActiveMqServerAdministrator implements ExceptionListener, MessageListener {

    private final Server server;
    private ActiveMQConnection connection;
    private ActiveMQSession session;
    private ActiveMQConnectionFactory connectionFactory;

    public ActiveMqServerAdministrator(Server server) {
        this.server = server;
    }

    public void prepare() {
        try {
            connectionFactory = new ActiveMQConnectionFactory("tcp://" + server.getHost() + ":" + server.getPort());
            connectionFactory.setUserName(server.getDefaultUsername());
            connectionFactory.setPassword(server.getDefaultPassword());
            connection = (ActiveMQConnection) connectionFactory.createConnection();
            connection.setExceptionListener(this);
            connection.start();
            session = (ActiveMQSession) connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            onException(e);
        }
    }

    public void createQueue(String name) {
        // Queue is created as soon as someone listens to it
        try {
            Queue queue = session.createQueue(name);
            MessageConsumer consumer = session.createConsumer(queue);
            consumer.close();
        } catch (JMSException e) {
            onException(e);
        }
    }


    public void createTopic(String name) {
        // Topic is created as soon as someone listens to it
        try {
            Topic topic = session.createTopic(name);
            MessageConsumer consumer = session.createConsumer(topic);
            consumer.close();
        } catch (JMSException e) {
            onException(e);
        }
    }

    public void deleteDestination(String name, DestinationType destinationType) {
        Inventory s;
        try {
            s = getInventory();
        } catch (JMSException e) {
            onException(e);
            return;
        }
        Optional<? extends ActiveMQDestination> destination = destinationType.isQueue() ? s.getQueueByName(name) : s.getTopicByName(name);
        destination.ifPresent(
                dest -> {
                    try {
                        connection.destroyDestination(dest);
                    } catch (JMSException e) {
                        onException(e);
                    }
                }
        );
    }

    public Inventory getInventory() throws JMSException {
        Set<ActiveMQQueue> queues = connection.getDestinationSource().getQueues();
        Set<ActiveMQTopic> topics = connection.getDestinationSource().getTopics();
        return new Inventory(queues, topics);
    }

    public void shutdown() throws JMSException { //TODO Exception necessary?
        session.close();
        connection.close();
    }

    @Override
    public synchronized void onException(JMSException ex) {
        log.error("Caught: ", ex);
        log.error("JMS Exception occured.  Shutting down client.");
    }

    @Override
    public void onMessage(Message message) {

    }

    public void refreshQueuesAndTopics() {
        server.getQueues().getQueues().clear();
        server.getTopics().getTopics().clear();
        try {
            Inventory inventory = getInventory();
            log.info("Found the following queues and topics: {}", inventory);
            server.getQueues().getQueues().addAll(inventory.getOrderedQueues(server.getQueues()));
            server.getTopics().getTopics().addAll(inventory.getOrderedTopics(server.getTopics()));
        } catch (JMSException e) {
            onException(e);
        }
    }
}
