package messagingtool.messaging.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.model.DestinationType;
import messagingtool.model.Server;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

@Slf4j
public class ActiveMqProducer implements ExceptionListener {

    private final Server server;
    private final String destinationName;
    private final DestinationType destinationType;
    private Connection connection;
    private Session session;
    private MessageProducer producer;

    public ActiveMqProducer(Server server, String destinationName, DestinationType destinationType) {
        this.server = server;
        this.destinationName = destinationName;
        this.destinationType = destinationType;
    }

    public void prepare() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + server.getHost() + ":" + server.getPort());
            connectionFactory.setUserName(server.getDefaultUsername());
            connectionFactory.setPassword(server.getDefaultPassword());
            connection = connectionFactory.createConnection();
            connection.setExceptionListener(this);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = destinationType.isQueue() ? session.createQueue(destinationName) : session.createTopic(destinationName);
            producer = session.createProducer(destination);
        } catch (JMSException e) {
            onException(e);
        }
    }

    public void send(Message msg) {
        try {
            TextMessage textMessage = session.createTextMessage();
            TextMessage textMsg = (TextMessage) msg;
            textMessage.setText(textMsg.getText());
            producer.send(textMessage);
        } catch (JMSException e) {
            log.error("Caught: ", e);
            onException(e);
        }
    }

    public void shutdown() {
        try {
            producer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            onException(e);
        }
    }

    @Override
    public synchronized void onException(JMSException ex) {
        log.error("Caught: ", ex);
        log.error("JMS Exception occured. Shutting down client.");
    }
}
