package messagingtool.messaging.activemq;

import lombok.ToString;
import messagingtool.model.Queue;
import messagingtool.model.Queues;
import messagingtool.model.Topic;
import messagingtool.model.Topics;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;

import javax.jms.JMSException;
import java.util.*;


@ToString
public class Inventory {
    private final Set<ActiveMQQueue> queues;
    private final Set<ActiveMQTopic> topics;

    public Inventory(Set<ActiveMQQueue> queues, Set<ActiveMQTopic> topics) {
        this.queues = queues;
        this.topics = topics;
    }

    public List<Queue> getOrderedQueues(Queues queues) {
        List<Queue> qs = new ArrayList<>();
        this.queues.forEach(q -> qs.add(new Queue(queues, q)));
        qs.sort(Comparator.comparing(Queue::getDisplayName));
        return qs;
    }

    public List<Topic> getOrderedTopics(Topics topics) {
        List<Topic> qs = new ArrayList<>();
        this.topics.forEach(t -> qs.add(new Topic(topics, t)));
        qs.sort(Comparator.comparing(Topic::getDisplayName));
        return qs;
    }

    public Optional<ActiveMQQueue> getQueueByName(String name) {
        return queues.stream().filter(q -> {
            try {
                return q.getQueueName().equals(name);
            } catch (JMSException e) {
                return false;
            }
        }).findFirst();
    }

    public Optional<ActiveMQTopic> getTopicByName(String name) {
        return topics.stream().filter(q -> {
            try {
                return q.getTopicName().equals(name);
            } catch (JMSException e) {
                return false;
            }
        }).findFirst();
    }
}
