package messagingtool.gui;

public interface RunnableWithException {
    void run() throws Exception;
}
