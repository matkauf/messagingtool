package messagingtool.gui.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.Execute;
import messagingtool.gui.LinkedBlockingQueueWithNotification;
import messagingtool.gui.component.ConfigurationJFrame;
import messagingtool.gui.component.ListComponent;
import messagingtool.gui.component.MessageComponent;
import messagingtool.messaging.activemq.ActiveMqConsumer;
import messagingtool.model.*;
import messagingtool.service.SpringService;
import org.apache.activemq.command.ActiveMQTextMessage;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.swing.*;
import java.awt.event.ActionEvent;

import static messagingtool.configuration.ApplicationConfig.DEBUGMODE;

@Slf4j
public class ActiveMQConsumerFrame extends ConfigurationJFrame {

    private final SpringService springService;
    private final Server server;
    private final String destinationName;
    private final DestinationType destinationType;
    private final LinkedBlockingQueueWithNotification<Message> messages = new LinkedBlockingQueueWithNotification<>();
    private final ConsumerConfigurations consumerConfigurations;
    private JCheckBox receiveToggle;
    private JLabel nameLabel;
    private JTextField nameInput;
    private ActiveMqConsumer consumer;
    private ListComponent msgListComponent;
    private MessageComponent messageComponent;
    private boolean listening = true;

    public ActiveMQConsumerFrame(SpringService springService, Server server, DestinationType destinationType, String destinationName) {
        super(springService);
        this.springService = springService;
        this.server = server;
        this.destinationName = destinationName;
        this.destinationType = destinationType;
        this.consumerConfigurations = (destinationType.isQueue() ? server.getQueues().getConsumerConfigurations() : server.getTopics().getConsumerConfigurations());
    }

    @Override
    protected void initGui() {
        setTitle("ActiveMQ - consumer for " + destinationType.getLowercaseDescription() + " " + destinationName);
        setSize(350, 200);

        nameLabel = new JLabel("Queue:");
        nameInput = new JTextField(destinationName);
        messageComponent = new MessageComponent(springService.getPrettyPrinter());
        msgListComponent = new ListComponent(messages, messageComponent);
        receiveToggle = new JCheckBox("Receive messages...");
        receiveToggle.addActionListener((ActionEvent event) -> receiveButtonPressed());

        containerAdd(nameLabel, 0, 0, 1, 1, false, true, false, false);
        containerAdd(nameInput, 1, 0, 2, 1, false, true, true, false);
        containerAdd(receiveToggle, 3, 0, 1, 1, false, false, true, false);
        containerAdd(msgListComponent, 0, 1, 2, 1, true, true, false, true);
        containerAdd(messageComponent, 2, 1, 2, 1, true, true, true, true);

        ConsumerConfiguration consumerConfiguration = consumerConfigurations.createOrGetByDestination(destinationName);
        setConfiguration(consumerConfiguration);
        messageComponent.load(consumerConfiguration);
        if (consumerConfiguration.isListening()) {
            receiveToggle.setSelected(true);
            receiveButtonPressed();
        }
    }

    private Message createMessage(String aaa) {
        try {
            TextMessage msg = new ActiveMQTextMessage();
            msg.setText(aaa);
            return msg;
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void cleanupResources() {
        String destinationName = getNormalizedDestinationName();
        if (destinationName !=null) {
            ConsumerConfiguration configuration = consumerConfigurations.createOrGetByDestination(destinationName);
            messageComponent.save(configuration);
            configuration.setListening(receiveToggle.isSelected());
            configuration.updateLocationAndSizeFrom(this);
        }
        if (consumer != null) {
            log.info("Shutdown consumer");
            consumer.stop();
            consumer.shutdown();
            consumer = null;
            listening = false;
            messages.clear();
        }
    }

    private void receiveButtonPressed() {
        String destinationName = nameInput.getText();
        if (destinationName == null || destinationName.isBlank()) {
            return;
        }
        nameInput.setEditable(false);
        if (consumer == null) {
            consumer = new ActiveMqConsumer(server, destinationName, destinationType, messages);
            if (DEBUGMODE) {
                messages.add(createMessage("AAA"));
                messages.add(createMessage("bbbb"));
                messages.add(createMessage("{\"anyAttribute\":123,\"aStringAttribute\":\"a test json message\"}"));
                messages.add(createMessage("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<anElement version=\"1.0\"><aSubElement authIdentifier=\"0\"/></anElement>"));
            }
            Execute.inWorkerThread(() -> {
                consumer.prepare();
                consumer.start();
                while (listening) {
                    messages.waitForUpdates();
                    Execute.delayedInMillisecs(msgListComponent, 250,
                            () -> Execute.inGuiThread(
                                    () -> msgListComponent.updateList()));
                }
                log.info("Consumer thread finished");
            });
            return;
        }
        if (receiveToggle.isSelected()) {
            consumer.start();
        } else {
            consumer.stop();
        }
    }

    private String getNormalizedDestinationName() {
        String destinationName = nameInput.getText();
        if (destinationName == null || destinationName.trim().isBlank()) {
            return null;
        }
        return destinationName.trim();
    }
}