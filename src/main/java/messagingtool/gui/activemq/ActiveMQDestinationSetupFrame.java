package messagingtool.gui.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.Execute;
import messagingtool.gui.component.AbstractJFrame;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.messaging.activemq.ActiveMqServerAdministrator;
import messagingtool.model.DestinationType;
import messagingtool.model.Server;
import messagingtool.service.SpringService;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static messagingtool.gui.component.InputValueAdjuster.getText;

@Slf4j
public class ActiveMQDestinationSetupFrame extends AbstractJFrame {

    private final MenuDelegator menuDelegator;
    private final Server server;
    private final boolean add;
    private JButton addUpdateButton;
    private JButton cancelButton;
    private JLabel nameLabel;
    private JTextField nameInput;
    private DestinationType destinationType;

    public ActiveMQDestinationSetupFrame(SpringService springService, MenuDelegator menuDelegator, Server server, DestinationType destinationType, boolean add) {
        super(springService);
        this.menuDelegator = menuDelegator;
        this.server = server;
        this.destinationType = destinationType;
        this.add = add;
    }

    @Override
    protected void initGui() {
        setTitle((add ? "Add" : "Configure") + " ActiveMQ " + destinationType.getUppercaseDescription());
        setSize(350, 200);

        nameLabel = new JLabel("Name:");
        nameInput = new JTextField("");
        addUpdateButton = new JButton(add ? "Add" : "Save");
        addUpdateButton.addActionListener((ActionEvent event) -> add());
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((ActionEvent event) -> cancel());

        containerAdd(nameLabel, 0, 0, 1, 1, false, false, true, false);
        containerAdd(nameInput, 1, 0, 2, 1, false, true, true, false);

        containerAdd(addUpdateButton, 0, 1, 2, 1, true, true, true, false);
        containerAdd(cancelButton, 2, 1, 1, 1, true, true, true, false);
    }

    @Override
    protected void cleanupResources() {
        //TODO
    }


    private void add() {
        addUpdateButton.setEnabled(false);
        Execute.inWorkerThread(() -> {
            String destinationName = getText(nameInput.getText(), destinationType.getSerializationDescription() + (System.nanoTime() % 10000L));
            ActiveMqServerAdministrator serverAdmin = new ActiveMqServerAdministrator(server);
            serverAdmin.prepare();
            if (destinationType.isQueue()) {
                serverAdmin.createQueue(destinationName);
            } else {
                serverAdmin.createTopic(destinationName);
            }
            serverAdmin.refreshQueuesAndTopics();
            serverAdmin.shutdown();
            Execute.inGuiThread(() -> {
                menuDelegator.refreshTree();
                setVisible(false);
            });
        });
    }

    private void cancel() {
        setVisible(false);
    }
}