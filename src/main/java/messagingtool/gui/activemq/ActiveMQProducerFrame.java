package messagingtool.gui.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.Execute;
import messagingtool.gui.component.ConfigurationJFrame;
import messagingtool.messaging.activemq.ActiveMqProducer;
import messagingtool.model.DestinationType;
import messagingtool.model.ProducerConfiguration;
import messagingtool.model.ProducerConfigurations;
import messagingtool.model.Server;
import messagingtool.service.SpringService;
import org.apache.activemq.command.ActiveMQTextMessage;

import javax.swing.*;
import java.awt.event.ActionEvent;

@Slf4j
public class ActiveMQProducerFrame extends ConfigurationJFrame {
    private final Server server;
    private final String destinationName;
    private final DestinationType destinationType;
    private final ProducerConfigurations producerConfigurations;
    private JButton sendButton;
    private JLabel nameLabel;
    private JTextField nameInput;
    private JTextArea textArea;
    private ActiveMqProducer producer;

    public ActiveMQProducerFrame(SpringService springService, Server server, DestinationType destinationType, String destinationName) {
        super(springService);
        this.server = server;
        this.destinationName = destinationName;
        this.destinationType = destinationType;
        this.producerConfigurations = (destinationType.isQueue() ? server.getQueues().getProducerConfigurations() : server.getTopics().getProducerConfigurations());
    }

    @Override
    protected void initGui() {
        setTitle("ActiveMQ - producer for " + destinationType.getLowercaseDescription() + " " + destinationName);
        setSize(350, 200);

        nameLabel = new JLabel("Queue:");
        nameInput = new JTextField(destinationName);
        textArea = new JTextArea("");
        sendButton = new JButton("Send");
        sendButton.addActionListener((ActionEvent event) -> send());

        containerAdd(nameLabel, 0, 0, 1, 1, false, false, true, false);
        containerAdd(nameInput, 1, 0, 2, 1, false, true, true, false);
        containerAdd(textArea, 0, 1, 3, 1, true, true, true, true);
        containerAdd(sendButton, 0, 2, 3, 1, false, true, true, false);

        ProducerConfiguration configuration = producerConfigurations.createOrGetByDestination(destinationName);
        setConfiguration(configuration);
        textArea.setText(configuration.getMessageText());
    }

    @Override
    protected void cleanupResources() {
        String destinationName = getNormalizedDestinationName();
        if (destinationName != null) {
            ProducerConfiguration configuration = producerConfigurations.createOrGetByDestination(destinationName);
            configuration.setMessageText(textArea.getText());
            configuration.updateLocationAndSizeFrom(this);
        }
        if (producer != null) {
            log.info("Shutdown producer");
            producer.shutdown();
            producer = null;
        }
    }

    private void send() {
        String destinationName = getNormalizedDestinationName();
        if (destinationName == null) {
            return;
        }
        nameInput.setEditable(false);
        sendButton.setEnabled(false);
        Execute.inWorkerThread(() -> {
            if (producer == null) {
                producer = new ActiveMqProducer(server, destinationName, destinationType);
                producer.prepare();
            }
            String msgText = textArea.getText();
            ActiveMQTextMessage textMessage = new ActiveMQTextMessage();
            textMessage.setText(msgText);
            log.info("Send message to {} {}", destinationType.getLowercaseDescription(), destinationName);
            producer.send(textMessage);
            Execute.inGuiThread(() -> {
                sendButton.setEnabled(true);
                // nameInput is not enabled again, because its current state is bound by the open connection to the broker
            });
        });
    }

    private String getNormalizedDestinationName() {
        String destinationName = nameInput.getText();
        if (destinationName == null || destinationName.trim().isBlank()) {
            return null;
        }
        return destinationName.trim();
    }
}