package messagingtool.gui.activemq;

import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.Execute;
import messagingtool.gui.component.AbstractJFrame;
import messagingtool.gui.component.MenuDelegator;
import messagingtool.model.Model;
import messagingtool.model.Server;
import messagingtool.service.SpringService;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static messagingtool.gui.component.InputValueAdjuster.getNumber;
import static messagingtool.gui.component.InputValueAdjuster.getText;

@Slf4j
public class ActiveMQBrokerSetupFrame extends AbstractJFrame {

    private final MenuDelegator menuDelegator;
    private final Model model;
    private final Server server;
    private final boolean add;
    private JButton addUpdateButton;
    private JButton cancelButton;
    private JLabel nameLabel;
    private JLabel hostLabel;
    private JLabel portLabel;
    private JLabel userLabel;
    private JLabel passLabel;
    private JTextField nameInput;
    private JTextField hostInput;
    private JTextField portInput;
    private JTextField userInput;
    private JPasswordField passInput;

    public ActiveMQBrokerSetupFrame(SpringService springService, MenuDelegator menuDelegator, Model model, Server server, boolean add) {
        super(springService);
        this.menuDelegator = menuDelegator;
        this.model = model;
        this.server = server;
        this.add = add;
    }

    @Override
    protected void initGui() {
        setTitle((add ? "Add" : "Configure") + " ActiveMQ server");
        setSize(350, 200);

        nameLabel = new JLabel("Name:");
        hostLabel = new JLabel("Host:");
        portLabel = new JLabel("Port:");
        userLabel = new JLabel("Default username:");
        passLabel = new JLabel("Default password:");
        nameInput = new JTextField(server.getName());
        hostInput = new JTextField(server.getHost());
        portInput = new JTextField(String.valueOf(server.getPort()));
        userInput = new JTextField(server.getDefaultUsername());
        passInput = new JPasswordField(server.getDefaultPassword());

        addUpdateButton = new JButton(add ? "Add" : "Save");
        addUpdateButton.addActionListener((ActionEvent event) -> add());
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((ActionEvent event) -> cancel());

        containerAdd(nameLabel, 0, 0, 1, 1, false, false, true, false);
        containerAdd(hostLabel, 0, 1, 1, 1, false, false, true, false);
        containerAdd(portLabel, 0, 3, 1, 1, false, false, true, false);
        containerAdd(userLabel, 0, 4, 1, 1, false, false, true, false);
        containerAdd(passLabel, 0, 5, 1, 1, false, false, true, false);

        containerAdd(nameInput, 1, 0, 2, 1, false, true, true, false);
        containerAdd(hostInput, 1, 1, 2, 1, false, true, true, false);
        containerAdd(portInput, 1, 3, 2, 1, false, true, true, false);
        containerAdd(userInput, 1, 4, 2, 1, false, true, true, false);
        containerAdd(passInput, 1, 5, 2, 1, false, true, true, false);

        containerAdd(addUpdateButton, 0, 6, 2, 1, true, true, true, false);
        containerAdd(cancelButton, 2, 6, 1, 1, true, true, true, false);
    }

    @Override
    protected void cleanupResources() {
        //TODO
    }


    private void add() {
        Execute.inWorkerThread(() -> {
            server.setName(getText(nameInput.getText(), ""));
            server.setHost(getText(hostInput.getText(), "xyz.ch"));
            server.setPort(getNumber(portInput.getText(), 61616));
            server.setDefaultUsername(getText(userInput.getText(), "abc"));
            server.setDefaultPassword(getText(new String(passInput.getPassword()), ""));
            if (add) {
                model.getServers().add(server);
            }
            Execute.inGuiThread(() -> {
                menuDelegator.refreshTree();
                setVisible(false);
            });
        });
    }

    private void cancel() {
        setVisible(false);
    }
}