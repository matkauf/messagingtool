package messagingtool.gui.component;

import messagingtool.model.AbstractConfiguration;
import messagingtool.service.SpringService;

public abstract class ConfigurationJFrame extends AbstractJFrame {
    private AbstractConfiguration configuration;

    public ConfigurationJFrame(SpringService springService) {
        super(springService);
    }

    protected void setConfiguration(AbstractConfiguration configuration) {
        this.configuration = configuration;
    }

    protected void setWindowSizeAndLocation() {
        if (configuration != null) {
            configuration.setLocationAndSizeTo(this);
        } else {
            super.setWindowSizeAndLocation();
        }
    }

}
