package messagingtool.gui.component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

@Setter
@Getter
public abstract class NodeElement<PARENTTYPE extends TreeNode, CHILDTYPE extends TreeNode> implements TreeNode {

    @JsonIgnore
    private PARENTTYPE parent;

    public NodeElement() { // used only for deserialization
    }

    @JsonIgnore
    public abstract String getDisplayName();

    @JsonIgnore
    public abstract List<CHILDTYPE> getChildren();

    @JsonIgnore
    public abstract boolean isLeaf();

    @JsonIgnore
    public abstract void createPopupMenu(PopupMenuCreator creator, MenuDelegator menuDelegator);

    @JsonIgnore
    public NodeElement(PARENTTYPE parent) {
        this.parent = parent;
    }

    @Override
    public CHILDTYPE getChildAt(int i) {
        return getChildren().get(i);
    }

    @Override
    public int getChildCount() {
        return getChildren().size();
    }

    @Override
    public PARENTTYPE getParent() {
        return parent;
    }

    @Override
    public int getIndex(TreeNode treeNode) {
        return getChildren().indexOf(treeNode);
    }

    @JsonIgnore
    @Override
    public boolean getAllowsChildren() {
        return isLeaf();
    }

    @Override
    public Enumeration<CHILDTYPE> children() {
        return new Vector<>(getChildren()).elements();
    }

}
