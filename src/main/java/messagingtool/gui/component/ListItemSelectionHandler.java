package messagingtool.gui.component;

import javax.jms.Message;

public interface ListItemSelectionHandler {

    void setMessageSelected(Message msg);
}