package messagingtool.gui.component;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@Slf4j
public class TreeMouseClickPopupMenu implements MouseListener {

    private final JTree tree;
    private final MenuDelegator menuDelegator;
    private final JPopupMenu popup;

    public TreeMouseClickPopupMenu(JTree tree, MenuDelegator menuDelegator) {
        this.tree = tree;
        this.menuDelegator = menuDelegator;
        popup = new JPopupMenu();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            int row = tree.getClosestRowForLocation(e.getX(), e.getY());
            tree.setSelectionRow(row);
            TreePath selectionPath = tree.getSelectionPath();
            NodeElement<? extends TreeNode, ? extends TreeNode> object = (NodeElement<? extends TreeNode, ? extends TreeNode>) selectionPath.getLastPathComponent();
            popup.removeAll();
            object.createPopupMenu(new PopupMenuCreator(popup, selectionPath, menuDelegator), menuDelegator);
            popup.show((JComponent) e.getSource(), e.getX(), e.getY());
            //log.info(object.getDisplayName() + " " + e.getX() + " " + e.getY());
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

}
