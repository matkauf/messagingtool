package messagingtool.gui.component;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class LayoutHelper {

    private static Font inputFont;
    private static final int INSET = 5;
    private static final int SHOW_RED_BORDER = 0;

    public static void addComponent(Container container, JComponent component, int x, int y, int w, int h, boolean toTop, boolean toLeft, boolean toRight, boolean toBottom) {
        if (component instanceof JTextField || component instanceof JTextArea) {
            inputFont = inputFont != null ? inputFont : new Font(Font.MONOSPACED, component.getFont().getStyle(), component.getFont().getSize());
            component.setFont(inputFont);
        }
        if (SHOW_RED_BORDER > 0) {
            component.setBorder(new LineBorder(Color.RED, 1, false));
        }
        boolean fillX = toLeft && toRight;
        boolean fillY = toTop && toBottom;
        GridBagConstraints gc = new GridBagConstraints();
        gc.insets = new Insets(INSET, INSET, INSET, INSET);

        switch ((toTop ? 1000 : 0) + (toLeft ? 100 : 0) + (toRight ? 10 : 0) + (toBottom ? 1 : 0)) {
            case 0:
                gc.anchor = GridBagConstraints.CENTER;
                break;
            case 1:
                gc.anchor = GridBagConstraints.SOUTH;
                break;
            case 10:
                gc.anchor = GridBagConstraints.EAST;
                break;
            case 11:
                gc.anchor = GridBagConstraints.SOUTHEAST;
                break;
            case 100:
                gc.anchor = GridBagConstraints.WEST;
                break;
            case 101:
                gc.anchor = GridBagConstraints.SOUTHWEST;
                break;
            case 110:
                gc.anchor = GridBagConstraints.CENTER;
                break;
            case 111:
                gc.anchor = GridBagConstraints.SOUTH;
                break;
            case 1000:
                gc.anchor = GridBagConstraints.NORTH;
                break;
            case 1001:
                gc.anchor = GridBagConstraints.CENTER;
                break;
            case 1010:
                gc.anchor = GridBagConstraints.NORTHEAST;
                break;
            case 1011:
                gc.anchor = GridBagConstraints.EAST;
                break;
            case 1100:
                gc.anchor = GridBagConstraints.NORTHWEST;
                break;
            case 1101:
                gc.anchor = GridBagConstraints.WEST;
                break;
            case 1110:
                gc.anchor = GridBagConstraints.NORTH;
                break;
            case 1111:
                gc.anchor = GridBagConstraints.CENTER;
                break;
            default:
                throw new IllegalStateException();
        }
        gc.ipadx = INSET;
        gc.ipady = INSET;
        gc.gridwidth = w;
        gc.gridheight = h;
        gc.gridx = x;
        gc.gridy = y;
        gc.weightx = fillX ? 1.0 : 0.0;
        gc.weighty = fillY ? 1.0 : 0.0;
        if (fillX && fillY) {
            gc.fill = GridBagConstraints.BOTH;
        } else if (fillY) {
            gc.fill = GridBagConstraints.VERTICAL;
        } else if (fillX) {
            gc.fill = GridBagConstraints.HORIZONTAL;
        } else {
            gc.fill = GridBagConstraints.NONE;
        }
        container.add(component, gc);
    }
}
