package messagingtool.gui.component;

import lombok.extern.slf4j.Slf4j;
import messagingtool.model.ConsumerConfiguration;
import messagingtool.service.PrettyPrinter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Enumeration;

/**
 * A component for the handling of a single received message.
 */
@Slf4j
public class MessageComponent extends JComponent implements ListItemSelectionHandler {

    private final PrettyPrinter prettyPrinter;
    private JCheckBox showPropertiesCheckbox;
    private JTextArea messageArea;
    private JLabel propertiesLabel;
    private JTextArea propertiesArea;
    private JLabel textTypeLabel;
    private ButtonGroup formatButtonGroup;
    private JRadioButton textButton;
    private JRadioButton jsonButton;
    private JRadioButton xmlButton;
    private String text;

    public MessageComponent(PrettyPrinter prettyPrinter) {
        this.prettyPrinter = prettyPrinter;
        initComponent();
    }

    private void initComponent() {
        GridBagLayout gc = new GridBagLayout();
        setLayout(gc);

        textTypeLabel = new JLabel("Message as ");
        formatButtonGroup = new ButtonGroup();
        textButton = new JRadioButton("Text/raw");
        jsonButton = new JRadioButton("JSON");
        xmlButton = new JRadioButton("XML:");
        formatButtonGroup.add(textButton);
        formatButtonGroup.add(jsonButton);
        formatButtonGroup.add(xmlButton);
        textButton.setSelected(true);

        messageArea = new JTextArea();
        messageArea.setEditable(false);
        showPropertiesCheckbox = new JCheckBox("Show message properties");
        showPropertiesCheckbox.setSelected(false);
        propertiesLabel = new JLabel("Message properties:");
        propertiesArea = new JTextArea("");
        propertiesArea.setEditable(false);
        propertiesLabel.setVisible(false);
        propertiesArea.setVisible(false);

        containerAdd(textTypeLabel, 0, 0, 1, 1, false, true, false, false);
        containerAdd(textButton, 1, 0, 1, 1, false, true, false, false);
        containerAdd(jsonButton, 2, 0, 1, 1, false, true, false, false);
        containerAdd(xmlButton, 3, 0, 1, 1, false, true, false, false);
        containerAdd(messageArea, 0, 1, 5, 1, true, true, true, true);
        containerAdd(propertiesLabel, 0, 2, 5, 1, false, true, true, true);
        containerAdd(propertiesArea, 0, 3, 5, 1, true, true, true, true);
        containerAdd(showPropertiesCheckbox, 4, 4, 1, 1, false, false, true, true);

        ActionListener actionListener = actionEvent -> setMessageAreaText();
        textButton.addActionListener(actionListener);
        jsonButton.addActionListener(actionListener);
        xmlButton.addActionListener(actionListener);
        showPropertiesCheckbox.addActionListener((ActionEvent e) -> showMessageHeaders());
    }

    private void showMessageHeaders() {
        boolean enabled = showPropertiesCheckbox.isSelected();
        propertiesLabel.setVisible(enabled);
        propertiesArea.setVisible(enabled);
    }

    protected void containerAdd(JComponent component, int x, int y, int w, int h, boolean toTop, boolean toLeft, boolean toRight, boolean toBottom) {
        LayoutHelper.addComponent(this, component, x, y, w, h, toTop, toLeft, toRight, toBottom);
    }

    @Override
    public void setMessageSelected(Message msg) {
        TextMessage textMessage = ((TextMessage) msg);
        try {
            text = textMessage.getText();
            setMessageAreaText();
            StringBuilder sb = new StringBuilder();
            Enumeration<?> propertyNames = textMessage.getPropertyNames();
            while (propertyNames.hasMoreElements()) {
                mapMessageProperty(sb, textMessage, (String) propertyNames.nextElement());
            }
            mapJmsProperty(sb, "JMSTimestamp", new Date(textMessage.getJMSTimestamp()));
            //mapTypedProperty(sb, "JMSDeliveryTime", new Date(textMessage.getJMSDeliveryTime())); not implemented
            mapJmsProperty(sb, "JMSCorrelationID", textMessage.getJMSCorrelationID());
            mapJmsProperty(sb, "JMSDeliveryMode", textMessage.getJMSDeliveryMode());
            mapJmsProperty(sb, "JMSDestination", textMessage.getJMSDestination());
            mapJmsProperty(sb, "JMSExpiration", new Date(textMessage.getJMSExpiration()));
            mapJmsProperty(sb, "JMSPriority", textMessage.getJMSPriority());
            mapJmsProperty(sb, "JMSRedelivered", textMessage.getJMSRedelivered());
            mapJmsProperty(sb, "JMSReplyTo", textMessage.getJMSReplyTo());
            mapJmsProperty(sb, "JMSType", textMessage.getJMSType());
            propertiesArea.setText(sb.toString());
        } catch (JMSException e) {
            throw new IllegalStateException(e);
        }
    }


    public void save(ConsumerConfiguration configuration) {
        configuration.setDisplayType(getType(textButton, "text") + getType(jsonButton, "json") + getType(xmlButton, "xml"));
    }

    public void load(ConsumerConfiguration consumerConfiguration) {
        switch (consumerConfiguration.getDisplayType()) {
            case "json": jsonButton.setSelected(true); break;
            case "xml": xmlButton.setSelected(true); break;
            default: textButton.setSelected(true); break;
        }
    }

    private void setMessageAreaText() {
        String adjustedText = text;
        if (jsonButton.isSelected()) {
            adjustedText = prettyPrinter.getJsonPrettyPrinted(text);
        } else if (xmlButton.isSelected()) {
            adjustedText = prettyPrinter.getXmlPrettyPrinted(text);
        }
        messageArea.setText(adjustedText);
    }

    private void mapJmsProperty(StringBuilder sb, String property, Object object) {
        if (object != null) {
            sb.append(property).append("=").append(object).append("\n");
        }
    }

    private void mapMessageProperty(StringBuilder sb, TextMessage textMessage, String name) throws JMSException {
        Object object = textMessage.getObjectProperty(name);
        if ((object != null && !(object instanceof String))) {
            sb.append("[").append(object.getClass().getSimpleName()).append("] ");
        }
        sb.append(name).append("=").append(object).append("\n");
    }


    private String getType(JRadioButton radioButton, String typeIfSelected) {
        return radioButton.isSelected() ? typeIfSelected : "";
    }
}
