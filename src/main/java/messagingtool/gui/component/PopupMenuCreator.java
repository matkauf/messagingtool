package messagingtool.gui.component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.event.ActionListener;

@AllArgsConstructor
@Getter
@Slf4j
public class PopupMenuCreator {

    private JPopupMenu popupMenu;
    private TreePath treePath;
    private MenuDelegator delegator;

    public final void createMenuItem(String text, boolean enabled, ActionListener actionListener) {
        JMenuItem popupMenuItem = new JMenuItem(text);
        popupMenuItem.setEnabled(enabled);
        if (enabled) {
            if (actionListener == null) {
                actionListener = actionEvent -> log.error("Not yet implemented: popup click on '{}'", text);
            }
            popupMenuItem.addActionListener(actionListener);
        }
        popupMenu.add(popupMenuItem);
    }
}
