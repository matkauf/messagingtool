package messagingtool.gui.component;

import lombok.Getter;
import messagingtool.model.AbstractConfiguration;
import messagingtool.service.ConfigurationService;
import messagingtool.service.SpringService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;


/**
 * <pre>
 *     Usage:
 *     AbstractJFrame f = new <Concrete>JFrame
 *     f.setWindowingService
 *     f.setVisible
 * </pre>
 */

public abstract class AbstractJFrame extends JFrame {

    private final WindowService windowService;
    private final ConfigurationService configurationService;
    private Container container;
    private static BufferedImage icon;

    @Getter
    private final SpringService springService;

    public AbstractJFrame(SpringService springService) {
        windowService = springService.getWindowService();
        configurationService = springService.getConfigurationService();
        this.springService = springService;
    }

    protected abstract void initGui();

    @Override
    public final void setVisible(boolean enable) {
        if (enable) {
            createAndInitContainerFromContentPane();
            initGui();
            setWindowSizeAndLocation();
            pack();
            addWindowListener(createDefaultResourceClosingWindowAdapter());
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setIconImage(icon);
            windowService.register(this);
            super.setVisible(true);
        } else {
            super.setVisible(false);
            windowService.deregister(this);
            configurationService.save();
            cleanupResources();
            dispose();
        }
    }

    protected void setWindowSizeAndLocation() {
        windowService.getPreviousWindowConfig(this).ifPresent(w -> w.setLocationAndSizeTo(this));
    }

    private WindowAdapter createDefaultResourceClosingWindowAdapter() {
        return new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cleanupResources();
            }
        };
    }

    protected abstract void cleanupResources();

    private void createAndInitContainerFromContentPane() {
        container = this.getContentPane();
        GridBagLayout gc = new GridBagLayout();
        container.setLayout(gc);
        setLocation(getDefaultLocation());
        setSize(getDefaultSize());
    }

    protected Point getDefaultLocation() {
        return new Point(0, 0);
    }

    protected Dimension getDefaultSize() {
        return new Dimension(200, 100);
    }


    protected void containerAdd(JComponent component, int x, int y, int w, int h, boolean toTop, boolean toLeft, boolean toRight, boolean toBottom) {
        LayoutHelper.addComponent(container, component, x, y, w, h, toTop, toLeft, toRight, toBottom);
    }


    static {
        icon = new BufferedImage(128, 128, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g = (Graphics2D) icon.getGraphics();
        g.setBackground(Color.BLACK);
        g.setPaint(new GradientPaint(0, 128, Color.RED, 128, 0, Color.BLUE));
        g.setStroke(new BasicStroke(1));
        for (int i = 0; i < 128 * 2; i++) {
            if (i / 16 % 2 == 0) {
                g.drawLine(0, i, i, 0);
            }
        }
        g.dispose();
    }


    @Override
    public void pack() {
        Point location = getLocation();
        Dimension dimension = getSize();
        super.pack();
        setSize(dimension);
        setLocation(location);
    }
}
