package messagingtool.gui.component;

public class InputValueAdjuster {


    public static int getNumber(String text, int defaultValue) {
        try {
            return Integer.parseInt(text.trim());
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }


    public static String getText(String text, String defaultValue) {
        text = text.trim();
        return text.isEmpty() ? defaultValue : text;
    }

}
