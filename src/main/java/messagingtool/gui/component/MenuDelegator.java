package messagingtool.gui.component;

import messagingtool.model.*;

public interface MenuDelegator {

    void createActiveMQBroker();

    void updateActiveMQBroker(Server server);

    void openActiveMQBroker(Server server);

    void closeActiveMQBroker(Server server);

    void createActiveMQQueue(Queues queues);

    void createActiveMQTopic(Topics topics);

    void createActiveMQQueueProducer(Queues parent, Queue queue);

    void createActiveMQQueueConsumer(Queues parent, Queue queue);

    void createActiveMQTopicProducer(Topics parent, Topic topic);

    void createActiveMQTopicConsumer(Topics parent, Topic topic);

    void refreshTree();

    void deleteActiveMQQueue(Queues queues, Queue queue);

    void deleteActiveMQTopic(Topics topics, Topic topic);

    void refreshActiveMQQueuesAndTopics(Server server);
}
