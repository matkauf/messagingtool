package messagingtool.gui.component;

import lombok.Getter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListItem {
    @Getter
    private final Message message;

    private static final SimpleDateFormat DATEFORMATTER = new SimpleDateFormat("HH:mm:ss.SSS");

    public ListItem(Message message) {
        this.message = message;
    }

    public String toString() {
        try {
            TextMessage msg = ((TextMessage) message);
            return (DATEFORMATTER.format(new Date(msg.getJMSTimestamp())) + " (TextMessage): " + msg.getText());
        } catch (JMSException e) {
            return "(Message)";
        }
    }
}
