package messagingtool.gui.component;

import lombok.extern.slf4j.Slf4j;

import javax.jms.Message;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A scrollable list with selectable/deletable items, with a button to empty the queue and a notifier for list changes.
 */
@Slf4j
public class ListComponent extends JComponent implements ListSelectionListener, KeyListener {

    private final LinkedBlockingQueue<Message> messages;
    private final ListItemSelectionHandler listItemSelectionHandler;
    private JList<ListItem> list;
    private JButton removeButton;
    private JButton removeAllButton;
    private JScrollPane scrollPane;
    private DefaultListModel<ListItem> listModel;


    public ListComponent(LinkedBlockingQueue<Message> messages, ListItemSelectionHandler listItemSelectionHandler) {
        this.messages = messages;
        this.listItemSelectionHandler = listItemSelectionHandler;
        initComponent();
    }

    private void initComponent() {
        GridBagLayout gc = new GridBagLayout();
        setLayout(gc);

        listModel = new DefaultListModel<>();
        list = new JList<>(listModel);
        list.addListSelectionListener(this);
        list.addKeyListener(this);
        scrollPane = new JScrollPane(list);
        removeButton = new JButton("Remove selected message(s)");
        removeAllButton = new JButton("Remove all");
        containerAdd(scrollPane, 0, 0, 1, 1, true, true, true, true);
        containerAdd(removeButton, 0, 1, 1, 1, false, true, true, false);
        containerAdd(removeAllButton, 0, 2, 1, 1, false, true, true, false);
        removeButton.setToolTipText("Press 'Del' to remove selected messages only");
        removeButton.addActionListener((ActionEvent event) -> removeSelectedMessages());
        removeAllButton.addActionListener((ActionEvent event) -> messages.clear()); // fast & concurrent -> does not have to be done in another thread
        updateList();
        setPreferredSize(new Dimension(300, 100)); // hack: prevents list from resizing when message is clicked
    }

    public void updateList() {
        Message[] messageArray = messages.toArray(new Message[0]);
        listModel.clear();
        listModel.addAll(Stream.of(messageArray).map(ListItem::new).collect(Collectors.toList()));
        list.repaint();
    }


    protected void containerAdd(JComponent component, int x, int y, int w, int h, boolean toTop, boolean toLeft, boolean toRight, boolean toBottom) {
        LayoutHelper.addComponent(this, component, x, y, w, h, toTop, toLeft, toRight, toBottom);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        ListItem selectedValue = list.getSelectedValue();
        if (!e.getValueIsAdjusting() && selectedValue != null) {
            listItemSelectionHandler.setMessageSelected(selectedValue.getMessage());
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_DELETE) {
            removeSelectedMessages();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void removeSelectedMessages() {
        messages.removeAll(list.getSelectedValuesList().stream().map(ListItem::getMessage).collect(Collectors.toList()));
    }
}
