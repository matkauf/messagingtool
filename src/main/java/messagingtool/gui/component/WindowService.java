package messagingtool.gui.component;

import lombok.extern.slf4j.Slf4j;
import messagingtool.preferences.Window;
import messagingtool.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class WindowService {

    @Autowired
    private ConfigurationService configurationService;

    private Map<AbstractJFrame, String> jFrames = new HashMap<>();

    public Optional<Window> getPreviousWindowConfig(AbstractJFrame frame) {
        return configurationService.getApplication().getWindow(getWindowName(frame));
    }

    public void register(AbstractJFrame frame) {
        String windowName = getWindowName(frame);
        if (jFrames.containsKey(frame)) {
            throw new IllegalArgumentException(windowName + " has already been registered");
        }
        jFrames.put(frame, windowName);
    }

    public void deregister(AbstractJFrame frame) {
        String windowName = getWindowName(frame);
        if (!jFrames.containsKey(frame)) {
            throw new IllegalArgumentException(windowName + " has not been registered");
        }
        configurationService.getApplication().getWindow(windowName).ifPresentOrElse(
                w -> w.getLocationAndSizeFrom(frame),
                () -> configurationService.getApplication().addWindow(windowName, frame));
        jFrames.remove(frame);
        frame.cleanupResources();
    }

    public void deregisterRemainingWindows() {
        List.copyOf(jFrames.keySet()).forEach(this::deregister);
    }


    private String getWindowName(AbstractJFrame frame) {
        return frame.getClass().getSimpleName();
    }
}
