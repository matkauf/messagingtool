package messagingtool.gui;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Execute {
    private static final Set<Object> timedRunnables = new HashSet<>();
    private static final ExecutorService executorService = Executors.newFixedThreadPool(8);
    private static final ScheduledExecutorService schedulerService = Executors.newScheduledThreadPool(4);

    public static void inWorkerThread(RunnableWithException runnable) {
        executorService.execute(
                () -> {
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        log.error("Error occurred:", e);
                    }
                }
        );
    }

    public static void inGuiThread(Runnable runnable) {
        SwingUtilities.invokeLater(runnable);
    }

    /**
     * Executes the given runnable within millisecs milliseconds, maybe earlier.
     * This method has no effect if it is called before an already scheduled runnable actually runs.
     * The timing is done for each given object separately.
     */
    public static void delayedInMillisecs(Object object, int millisecs, RunnableWithException runnable) {
        synchronized (timedRunnables) {
            if (timedRunnables.contains(object)) {
                return;
            }
            timedRunnables.add(object);
        }
        schedulerService.schedule(
                () -> {
                    synchronized (timedRunnables) {
                        timedRunnables.remove(object);
                    }
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        log.error("Error occurred:", e);
                    }
                }, millisecs, TimeUnit.MILLISECONDS);
    }
}
