package messagingtool.gui;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class LinkedBlockingQueueWithNotification<E> extends LinkedBlockingQueue<E> {
    private final Semaphore semaphore = new Semaphore(0); // holds a permit for a gui update, initially released
    private final AtomicBoolean hasUpdated = new AtomicBoolean(false);


    public boolean offer(E element) {
        boolean result = super.offer(element);
        releaseIfLocked();
        return result;
    }

    public E take() throws InterruptedException {
        E take = super.take();
        releaseIfLocked();
        return take;
    }

    public void clear() {
        super.clear();
        releaseIfLocked();
    }

    public boolean removeAll(Collection<?> elements) {
        boolean result = super.removeAll(elements);
        releaseIfLocked();
        return result;
    }

    public void waitForUpdates() throws InterruptedException {
        try {
            semaphore.acquire();
            hasUpdated.set(false);
        } catch (InterruptedException e) {
            throw e;
        }
    }

    private void releaseIfLocked() {
        if (!hasUpdated.getAndSet(true)) {
            semaphore.release();
        }
    }
}