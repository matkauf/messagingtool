package messagingtool.gui;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import messagingtool.gui.component.NodeElement;
import messagingtool.model.Model;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class BrokerTreeModel implements TreeModel {

    private Model model;

    @Override
    public Object getRoot() {
        return model;
    }

    @Override
    public Object getChild(Object o, int i) {
        NodeElement ne = (NodeElement) o;
        return ne.getChildren().get(i);
    }

    @Override
    public int getChildCount(Object o) {
        NodeElement ne = (NodeElement) o;
        return ne.getChildren().size();
    }

    @Override
    public boolean isLeaf(Object o) {
        NodeElement ne = (NodeElement) o;
        return ne.isLeaf();
    }

    @Override
    public void valueForPathChanged(TreePath treePath, Object o) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        NodeElement<? extends TreeNode, ? extends TreeNode> p = (NodeElement<? extends TreeNode, ? extends TreeNode>) parent;
        NodeElement<? extends TreeNode, ? extends TreeNode> c = (NodeElement<? extends TreeNode, ? extends TreeNode>) child;
        List<?> children = p.getChildren();
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i) == c) {
                return 0;
            }
        }
        throw new IllegalStateException();
    }

    @Override
    public void addTreeModelListener(TreeModelListener treeModelListener) {
        throw new IllegalStateException();

    }

    @Override
    public void removeTreeModelListener(TreeModelListener treeModelListener) {
        throw new IllegalStateException();

    }
}
