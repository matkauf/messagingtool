package messagingtool.gui;

import messagingtool.gui.component.WindowMenuDelegator;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import static messagingtool.configuration.ApplicationConfig.DEBUGMODE;


class MenuBarFactory implements MenuListener {

    private final WindowMenuDelegator windowMenuDelegator;

    public MenuBarFactory(WindowMenuDelegator windowMenuDelegator) {
        this.windowMenuDelegator = windowMenuDelegator;
    }

    private JMenu createEditMenu() {
        JMenu editMenu = new JMenu("Edit");
        JMenuItem cutItem = new JMenuItem("Cut");
        editMenu.add(cutItem);
        JMenuItem copyItem = new JMenuItem("Copy");
        editMenu.add(copyItem);
        JMenuItem pasteItem = new JMenuItem("Paste");
        editMenu.add(pasteItem);
        return editMenu;
    }

    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem newItem = new JMenuItem("New");
        fileMenu.add(newItem);
        JMenuItem openItem = new JMenuItem("Open");
        fileMenu.add(openItem);
        JMenuItem saveItem = new JMenuItem("Save");
        fileMenu.add(saveItem);
        return fileMenu;
    }

    private JMenu createAddMenu() {
        JMenu fileMenu = new JMenu("Add");
        JMenuItem newItem = new JMenuItem("ActiveMQ Broker... (remote)");
        fileMenu.add(newItem);
        JMenuItem embed = new JMenuItem("ActiveMQ Broker... (embedded)");
        fileMenu.add(embed);
        JMenuItem openItem = new JMenuItem("WebsphereMQ Broker...");
        fileMenu.add(openItem);
        return fileMenu;
    }

    private JMenu createViewMenu() {
        JMenu fileMenu = new JMenu("View");
        JMenuItem newItem = new JMenuItem("Logs");
        fileMenu.add(newItem);
        return fileMenu;
    }

    private JMenu createDebugMenu() {
        JMenu debugMenu = new JMenu("Debug");
        JMenuItem newItem = new JMenuItem("Start embedded server and consumer");
        debugMenu.add(newItem);
        debugMenu.addMenuListener(this);
        return debugMenu;
    }

    public JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(createFileMenu());
        menuBar.add(createEditMenu());
        menuBar.add(createAddMenu());
        menuBar.add(createViewMenu());
        if (DEBUGMODE) {
            menuBar.add(createDebugMenu());
        }
        return menuBar;
    }

    @Override
    public void menuSelected(MenuEvent menuEvent) {
        windowMenuDelegator.debugCall();
    }

    @Override
    public void menuDeselected(MenuEvent menuEvent) {

    }

    @Override
    public void menuCanceled(MenuEvent menuEvent) {

    }

}
