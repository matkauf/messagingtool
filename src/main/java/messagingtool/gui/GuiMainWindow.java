package messagingtool.gui;

import lombok.extern.slf4j.Slf4j;
import messagingtool.gui.activemq.ActiveMQBrokerSetupFrame;
import messagingtool.gui.activemq.ActiveMQConsumerFrame;
import messagingtool.gui.activemq.ActiveMQDestinationSetupFrame;
import messagingtool.gui.activemq.ActiveMQProducerFrame;
import messagingtool.gui.component.*;
import messagingtool.messaging.activemq.ActiveMqEmbeddedServer;
import messagingtool.messaging.activemq.ActiveMqServerAdministrator;
import messagingtool.model.*;
import messagingtool.service.ConfigurationService;
import messagingtool.service.SpringService;
import org.apache.kafka.common.utils.Utils;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class GuiMainWindow extends AbstractJFrame implements MenuDelegator, WindowMenuDelegator, TreeWillExpandListener {

    private final ConfigurationService configurationService;
    private final WindowService windowService;
    private final Model model;
    private JScrollPane treeScrollPane;
    private JTree tree;
    private final MenuBarFactory menuBarFactory;

    public GuiMainWindow(SpringService springService) {
        super(springService);
        configurationService = springService.getConfigurationService();
        windowService = springService.getWindowService();
        Model m = configurationService.getApplication().getModel();
        model = m == null ? new Model() : m.prepareAfterDeserialization();
        configurationService.getApplication().setModel(model);
        menuBarFactory = new MenuBarFactory(this);
    }

    @Override
    protected void initGui() {
        setTitle("Messaging Tool 0.1");
        setSize(350, 200);
        setJMenuBar(menuBarFactory.createMenuBar());

        tree = new JTree(model) {
            @Override
            public String convertValueToText(Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                return ((NodeElement<? extends TreeNode, ? extends TreeNode>) value).getDisplayName();
            }
        };
        tree.addMouseListener(new TreeMouseClickPopupMenu(tree, this));
        treeScrollPane = new JScrollPane(tree);
        tree.setRootVisible(true);
        tree.addTreeWillExpandListener(this);

        containerAdd(treeScrollPane, 0, 0, 1, 1, true, true, true, true);
    }

    @Override
    protected void cleanupResources() {
        windowService.deregisterRemainingWindows();
        configurationService.save();
        System.exit(0);
    }

    @Override
    public void refreshTree() {
        tree.updateUI();
    }

    @Override
    public void createActiveMQBroker() {
        Server server = new Server(model, ServerType.ACTIVEMQ);
        server.setHost("Hint: Use 'localhost' for an embedded server");
        ActiveMQBrokerSetupFrame frame = new ActiveMQBrokerSetupFrame(getSpringService(), this, model, server, true);
        frame.setVisible(true);
    }

    @Override
    public void deleteActiveMQQueue(Queues queues, Queue queue) {
        Execute.inWorkerThread(
                () -> {
                    ActiveMqServerAdministrator administrator = new ActiveMqServerAdministrator(queues.getParent());
                    administrator.prepare();
                    administrator.deleteDestination(queue.getName(), DestinationType.Queue);
                    log.info("Queue {} deleted", queue.getName());
                    administrator.refreshQueuesAndTopics();
                    administrator.shutdown();
                    Execute.inGuiThread(this::refreshTree);
                }
        );
    }

    @Override
    public void deleteActiveMQTopic(Topics topics, Topic topic) {
        Execute.inWorkerThread(
                () -> {
                    ActiveMqServerAdministrator administrator = new ActiveMqServerAdministrator(topics.getParent());
                    administrator.prepare();
                    administrator.deleteDestination(topic.getName(), DestinationType.Topic);
                    log.info("Topic {} deleted", topic.getName());
                    administrator.refreshQueuesAndTopics();
                    administrator.shutdown();
                    Execute.inGuiThread(this::refreshTree);
                }
        );
    }

    @Override
    public void openActiveMQBroker(Server server) {
        Execute.inWorkerThread(() -> {
            if (server.isEmbedded()) {
                log.info("Starting {}...", server.getHostAndPort());
                ActiveMqEmbeddedServer es = new ActiveMqEmbeddedServer();
                es.start(server);
                server.setActiveMqEmbeddedServer(es);
                log.info("{} started", server.getHostAndPort());
            }
            ActiveMqServerAdministrator administrator = new ActiveMqServerAdministrator(server);
            administrator.prepare();
            administrator.refreshQueuesAndTopics();
            administrator.shutdown();
            server.setConnected(true);
            Execute.inGuiThread(() -> {
                TreePath serverPath = tree.getSelectionPath();
                TreePath queuesPath = serverPath.pathByAddingChild(server.getQueues());
                TreePath topicsPath = serverPath.pathByAddingChild(server.getTopics());
                tree.expandPath(serverPath);
                tree.expandPath(queuesPath);
                tree.expandPath(topicsPath);
                treeScrollPane.updateUI();
            });
        });
    }

    @Override
    public void closeActiveMQBroker(Server server) {
        Execute.inWorkerThread(() -> {
            ActiveMqEmbeddedServer embeddedServer = server.getActiveMqEmbeddedServer();
            if (embeddedServer != null) {
                log.info("Stopping {}...", server.getHostAndPort());
                embeddedServer.shutdown();
                server.setActiveMqEmbeddedServer(null);
                log.info("{} stopped", server.getHostAndPort());
            }
            server.getQueues().getQueues().clear();
            server.getTopics().getTopics().clear();
            server.setConnected(false);
            Execute.inGuiThread(() -> {
                server.getQueues().getQueues().clear();
                server.getTopics().getTopics().clear();
                tree.collapsePath(getPath(server));
                treeScrollPane.updateUI();
            });
        });
    }

    @Override
    public void createActiveMQQueue(Queues queues) {
        ActiveMQDestinationSetupFrame frame = new ActiveMQDestinationSetupFrame(getSpringService(), this, queues.getParent(), DestinationType.Queue, true);
        frame.setVisible(true);
    }

    @Override
    public void createActiveMQTopic(Topics topics) {
        ActiveMQDestinationSetupFrame frame = new ActiveMQDestinationSetupFrame(getSpringService(), this, topics.getParent(), DestinationType.Topic, true);
        frame.setVisible(true);
    }

    @Override
    public void createActiveMQQueueProducer(Queues queues, Queue queue) {
        ActiveMQProducerFrame frame = new ActiveMQProducerFrame(getSpringService(), queues.getParent(), DestinationType.Queue, queue.getName());
        frame.setVisible(true);
    }

    @Override
    public void createActiveMQQueueConsumer(Queues queues, Queue queue) {
        ActiveMQConsumerFrame frame = new ActiveMQConsumerFrame(getSpringService(), queues.getParent(), DestinationType.Queue, queue.getName());
        frame.setVisible(true);
    }

    @Override
    public void createActiveMQTopicProducer(Topics topics, Topic topic) {
        ActiveMQProducerFrame frame = new ActiveMQProducerFrame(getSpringService(), topics.getParent(), DestinationType.Topic,topic.getName());
        frame.setVisible(true);
    }

    @Override
    public void createActiveMQTopicConsumer(Topics topics, Topic topic) {
        ActiveMQConsumerFrame frame = new ActiveMQConsumerFrame(getSpringService(), topics.getParent(), DestinationType.Topic,topic.getName());
        frame.setVisible(true);
    }

    @Override
    public void updateActiveMQBroker(Server server) {
        if (server.getServerType() != ServerType.ACTIVEMQ) {
            throw new IllegalStateException();
        }
        ActiveMQBrokerSetupFrame frame = new ActiveMQBrokerSetupFrame(getSpringService(), this, model, server, false);
        frame.setVisible(true);
    }

    @Override
    public void refreshActiveMQQueuesAndTopics(Server server) {
        Execute.inWorkerThread(() -> {
            ActiveMqServerAdministrator administrator = new ActiveMqServerAdministrator(server);
            administrator.prepare();
            administrator.refreshQueuesAndTopics();
            Execute.inGuiThread(() -> {
                tree.updateUI();
                treeScrollPane.updateUI();
            });
            administrator.shutdown();
        });
    }

    @Override
    public void treeWillExpand(TreeExpansionEvent treeExpansionEvent) throws ExpandVetoException {
        Object element = treeExpansionEvent.getPath().getLastPathComponent();
        if (!(element instanceof Server)) {
            return;
        }
        Server server = (Server) element;
        if (!server.isConnected()) {
            throw new ExpandVetoException(treeExpansionEvent);
        }
    }

    @Override
    public void treeWillCollapse(TreeExpansionEvent treeExpansionEvent) {

    }

    @Override
    public void debugCall() {
        openActiveMQBroker(model.getChildAt(2));
        Utils.sleep(2000);
        this.createActiveMQQueueConsumer(model.getChildAt(2).getQueues(), model.getChildAt(2).getQueues().getChildAt(0));
    }

    private TreePath getPath(TreeNode treeNode) {
        List<Object> nodes = new ArrayList<>();
        if (treeNode != null) {
            nodes.add(treeNode);
            treeNode = treeNode.getParent();
            while (treeNode != null) {
                nodes.add(0, treeNode);
                treeNode = treeNode.getParent();
            }
        }

        return nodes.isEmpty() ? null : new TreePath(nodes.toArray());
    }

    public static void startGui(SpringService service) {
        GuiMainWindow gmw = new GuiMainWindow(service);
        SwingUtilities.invokeLater(() -> gmw.setVisible(true));
    }
}