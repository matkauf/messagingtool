package messagingtool;

import messagingtool.gui.GuiMainWindow;
import messagingtool.service.SpringService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MessagingTool {

    public static void main(String[] args) {
        //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        ConfigurableApplicationContext ctx = new SpringApplicationBuilder(MessagingTool.class).headless(false).run(args);
        SpringService springService = ctx.getBean(SpringService.class);
        springService.getConfigurationService().load();
        GuiMainWindow.startGui(springService);
    }
}