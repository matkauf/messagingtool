# Project structure

### Directories
* configuration: Contains Spring configuration beans
* fileio: For producer/consumers reading from/writing to files
* gui: Gui components
* messaging: Worker classes for handling different types of brokers
* preferences: Model classes to save the current window/application state
* service: Some Spring initialized services
    

### Guides
